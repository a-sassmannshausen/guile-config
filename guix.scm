(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix gexp)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo)
  (srfi srfi-1))

(package
  (name "guile-config")
  (version "0.5.1")
  (source
    (local-file
      (dirname (current-filename))
      #:recursive?
      #t
      #:select?
      (λ (file stat)
         (not (any (lambda (my-string)
                     (string-contains file my-string))
                   (list ".git" ".dir-locals.el" "guix.scm"))))))
  (build-system gnu-build-system)
  (arguments `())
  (native-inputs
    (list autoconf automake pkg-config texinfo))
  (inputs (list guile-3.0))
  (propagated-inputs (list))
  (synopsis
    "Guile application configuration parsing library.")
  (description
    "Guile Config is a library providing a declarative approach to application configuration specification.  The library provides clean configuration declaration forms, and processors that take care of: configuration file creation; configuration file parsing; command-line parameter parsing using getopt-long; basic GNU command-line parameter generation (--help, --usage, --version); automatic output generation for the above command-line parameters.")
  (home-page
    "https://gitlab.com/a-sassmannshausen/guile-config")
  (license license:gpl3+))

