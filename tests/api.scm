;; tests/api.scm --- api implementation    -*- coding: utf-8 -*-
;;
;; Copyright (C) 2021 Alex Sassmannshausen <alex@pompo.co>
;;
;; Author: Alex Sassmannshausen <alex@pompo.co>
;;
;; This file is part of guile-config.
;;
;; guile-config is free software; you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the Free
;; Software Foundation; either version 3 of the License, or (at your option)
;; any later version.
;;
;; guile-config is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-config; if not, contact:
;;
;; Free Software Foundation           Voice:  +1-617-542-5942
;; 59 Temple Place - Suite 330        Fax:    +1-617-542-2652
;; Boston, MA  02111-1307,  USA       gnu@gnu.org

;;; Commentary:
;;
;;; Code:

(define-module (tests api)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (config api)
  #:use-module (srfi srfi-64)
  #:use-module (tests quickcheck)
  #:use-module (tests quickcheck-defs))

(test-begin "api")

;;;; Basics

(quickcheck-assert "secret?"
                   secret? $secret)

(quickcheck-assert "switch?"
                   switch? $switch)

(quickcheck-assert "setting?"
                   setting? $setting)

(quickcheck-assert "argument?"
                   argument? $argument)

;; Mandatory or optional
(test-assert "mandatory argument"
  (not (argument-optional? (argument (name 'test) (default (empty))))))

(test-assert "optional argument"
  (argument-optional? (argument (name 'test) (default ($string)))))

(quickcheck-assert "mandatory secret?"
                   keyword-mandatory? $secret)

(quickcheck-assert "optional setting?"
                   (negate keyword-mandatory?) $setting)

(quickcheck-assert "mandatory/optional switch?"
                   (λ (s)
                     (if (empty? (switch-default s))
                         (keyword-mandatory? s)
                         (not (keyword-mandatory? s))))
                   $switch-rich)

(test-end "api")
