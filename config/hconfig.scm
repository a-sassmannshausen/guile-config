(define-module
  (config hconfig)
  #:use-module
  (srfi srfi-26)
  #:export
  (%version
    %author
    %license
    %copyright
    %gettext-domain
    G_
    N_
    init-nls
    init-locale))

(define %version "0.5.1")

(define %author "Alex Sassmannshausen")

(define %license 'gpl3+)

(define %copyright
  '(2016 2017 2018 2020 2021 2022))

